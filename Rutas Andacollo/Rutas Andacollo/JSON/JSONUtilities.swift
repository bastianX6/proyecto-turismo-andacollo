//
//  JSONUtilities.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import Foundation

class JSONUtilities
{
    class func getLongValue(clave : String, json : JSON) -> NSNumber?
    {
        if(json[clave] != nil)
        {
            return json[clave]!.numberValue
        }
        
        return nil
    }
    
    class func getDoubleValue(clave : String, json : JSON) -> Double?
    {
        if(json[clave] != nil)
        {
            return json[clave]!.doubleValue
        }
        
        return nil
    }
    
    class func getStringValue(clave : String, json : JSON) -> String?
    {
        if(json[clave] != nil)
        {
            return json[clave]!.stringValue
        }
        
        return nil
    }
    
    class func getBoolValue(clave : String, json : JSON) -> Bool
    {
        if(json[clave] != nil)
        {
            return json[clave]!.boolValue
        }
        
        return false
    }
    
    class func getArrayValue(clave : String, json : JSON) -> Array<JSON>?
    {
        if(json[clave] != nil)
        {
            return json[clave]!.arrayValue
        }
        
        return nil
    }

}