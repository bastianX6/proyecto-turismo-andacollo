//
//  JSONColegio.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import Foundation

class JSONColegio
{
    
    class func getColegio(json : JSON) -> Colegio
    {
        var colegio = Colegio()
        
        
        colegio.anio = JSONUtilities.getLongValue("agno",json: json)
        colegio.nombreEstablecimiento = JSONUtilities.getStringValue("nombre_establecimiento",json: json)
        colegio.nombreComuna = JSONUtilities.getStringValue("nombre_comuna",json: json)
        colegio.dependencia = JSONUtilities.getStringValue("dependencia",json: json)
        colegio.estado = JSONUtilities.getStringValue("estado",json: json)
        colegio.latitud = JSONUtilities.getDoubleValue("latitud",json: json)
        colegio.longitud = JSONUtilities.getDoubleValue("longitud",json: json)
        colegio.simceLenguaje = JSONUtilities.getLongValue("simce_leng",json: json)
        colegio.simceMatematicas = JSONUtilities.getLongValue("simce_mate",json: json)
        
        
        return colegio
    }
    
    class func getArrayColegios(json : JSON) -> Array<Colegio>
    {
        var array : Array<Colegio> = []
        
        if let listaJson = JSONUtilities.getArrayValue("lista", json: json)
        {
            for colegioJson in listaJson
            {
                var colegio = self.getColegio(colegioJson)
                array.append(colegio)
            }
        }
        
        return array
    }
}