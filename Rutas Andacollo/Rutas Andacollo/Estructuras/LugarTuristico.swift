//
//  LugarTuristico.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import Foundation

class LugarTuristico : NSObject, Equatable
{
    var nombre : String?
    var descripcion : String?
    var direccion : String?
    var numeroTelefono : String?
    var paginaWeb : String?
    var email : String?
    var categoria : CategoriaLugar = .DESCONOCIDO
    //UBICACION EN MAPA
    
    override init()
    {
        
    }
}

func ==(lhs: LugarTuristico, rhs: LugarTuristico) -> Bool
{
    return true
}