//
//  CategoriaLugar.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import Foundation

public enum CategoriaLugar {
    
    case HOSPEDAJE, FAMILIAR ,SERVICIOS , GASTRONOMICA, RURAL, PATRIMONIAL, DESCONOCIDO
}