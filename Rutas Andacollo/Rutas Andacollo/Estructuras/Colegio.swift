//
//  Colegio.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import UIKit
import MapKit

class Colegio: NSObject {
    
    var anio : NSNumber?
    var nombreEstablecimiento : String?
    var nombreComuna : String?
    var dependencia : String?
    var estado : String?
    var latitud : Double?
    var longitud : Double?
    var simceLenguaje : NSNumber?
    var simceMatematicas : NSNumber?

    
    func getLocation() -> CLLocationCoordinate2D?
    {
        if latitud != nil && longitud != nil
        {
            var location = CLLocationCoordinate2D(latitude: latitud!, longitude: longitud!)
            return location
        }
        
        return nil
    }
}
