//
//  GetColegios.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import Foundation

class GetColegios
{
    
    var listaColegios : Array<Colegio> = []
    var listaComunas : Array<String> = []
    
    init()
    {
        let bundle = NSBundle.mainBundle()
        
        let pathJson = bundle.pathForResource("lista_colegios", ofType: "json")!
        
        var json : JSON = JSON(rutaJSON: pathJson, boleanoRelleno: true)
        
        self.listaColegios = JSONColegio.getArrayColegios(json)
        var setComunas : NSMutableSet = NSMutableSet()
        
        for colegio in listaColegios
        {
            setComunas.addObject(colegio.nombreComuna!)
        }
        
        self.listaComunas = setComunas.allObjects as! [String]
    }
    
    
    func getColegiosFromComuna(nombreComuna : String) -> Array<Colegio>
    {
        var array : Array<Colegio> = []
        
        for colegio in self.listaColegios
        {
            if colegio.nombreComuna?.lowercaseString == nombreComuna.lowercaseString
            {
                array.append(colegio)
            }
        }
        
        return array
    }
}