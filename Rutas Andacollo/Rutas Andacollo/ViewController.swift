//
//  ViewController.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionPerformed(sender: UIButton) {
        
        //var colegios = GetColegios()
        
        var storyBoard = UIStoryboard(name: "StoryboardPrincipal", bundle: nil)
        
        var leftController = storyBoard.instantiateViewControllerWithIdentifier("NavControllerLeft") as! UINavigationController
        var mainController = storyBoard.instantiateViewControllerWithIdentifier("NavControllerCenterMapa") as! UINavigationController
        
        if let controllerLista = leftController.viewControllers.first as? ControllerSeleccionarCategoria
        {
            println("Encontró view controller")
            //controllerLista.colegios = colegios
        }
        
        
        var app = UIApplication.sharedApplication().delegate
        
        var slideMenuController = SlideMenuController(mainViewController: mainController, leftMenuViewController: leftController)
        
        self.presentViewController(slideMenuController, animated: true, completion: nil)

        
        /*var titulo = "Cargando datos"
        var mensaje = "Espere un momento"
        
        var alert = UIAlertController(title: titulo, message: mensaje, preferredStyle: .Alert)
        
        self.presentViewController(alert, animated: true){
            var queue = NSOperationQueue()
            
            queue.addOperationWithBlock(){
                
                
                
                
            }
            
        }*/
    }

}

