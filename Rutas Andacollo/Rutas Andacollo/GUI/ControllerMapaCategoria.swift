//
//  ControllerMapaCategoria.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import UIKit
import MapKit

class ControllerMapaCategoria: UIViewController, MKMapViewDelegate {

    @IBOutlet var mapView: MKMapView!
    var listaColegios : Array<Colegio> = []
    var nombreComuna : String?
    @IBOutlet var navItem: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mapView.delegate = self
        self.mapView.mapType = .Standard
        self.navItem.title = nombreComuna
        self.loadAnotations()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func setUbicacionActualEnMapa(sender: UIBarButtonItem) {
    }
    
    
    @IBAction func cambiarEstiloMapaRutas(sender: UIBarButtonItem) {
        self.mapView.mapType = .Standard
    }
    
    @IBAction func cambiarEstiloMapaGeografico(sender: UIBarButtonItem) {
        self.mapView.mapType = MKMapType.Satellite
    }
    
    
    
    @IBAction func openMenu(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func openControllerDetalles(sender: UIBarButtonItem) {
        
        var storyboard = UIStoryboard(name: "StoryboardPrincipal", bundle: nil)
        var controller = storyboard.instantiateViewControllerWithIdentifier("ControllerDetalleCategoria") as! ControllerDetalleCategoria
        
        controller.listaColegios = self.listaColegios
        controller.nombreComuna = self.nombreComuna
        
        var mainController = self.slideMenuController()?.mainViewController as! UINavigationController
        
        mainController.setViewControllers([controller], animated: true)
    
    }
    
    func loadAnotations()
    {
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        if self.listaColegios.count > 0
        {

            var location = self.listaColegios[0].getLocation()!
            var span = MKCoordinateSpanMake(0.5, 0.5)
            var region = MKCoordinateRegion(center: location, span: span)
            self.mapView.setRegion(region, animated: true)
            
            var arrayAnotations : [MKPointAnnotation] = []
            
            for colegio in self.listaColegios
            {
                if let location : CLLocationCoordinate2D = colegio.getLocation()
                {
                    var annotation = MKPointAnnotation()
                    annotation.coordinate = location
                    annotation.title = colegio.nombreEstablecimiento
                    annotation.subtitle = "Latitud: \(colegio.latitud!), Longitud: \(colegio.longitud!)"
                    arrayAnotations.append(annotation)
                }
            }
            
            self.mapView.addAnnotations(arrayAnotations)
            
        }
    }

}
