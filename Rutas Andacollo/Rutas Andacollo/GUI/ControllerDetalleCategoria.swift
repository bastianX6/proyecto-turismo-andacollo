//
//  ControllerDetalleCategoria.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import UIKit

class ControllerDetalleCategoria: UITableViewController {

    var listaColegios : Array<Colegio> = []
    var nombreComuna : String?
    
    @IBOutlet var navItem: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navItem.title = nombreComuna
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func openMenu(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func openControllerMapa(sender: UIBarButtonItem) {
        
        var storyboard = UIStoryboard(name: "StoryboardPrincipal", bundle: nil)
        var controller = storyboard.instantiateViewControllerWithIdentifier("ControllerMapaCategoria") as! ControllerMapaCategoria
        
        controller.listaColegios = self.listaColegios
        controller.nombreComuna = self.nombreComuna
        
        var mainController = self.slideMenuController()?.mainViewController as! UINavigationController
        
        mainController.setViewControllers([controller], animated: true)
        
    }


    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return listaColegios.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as! UITableViewCell
        
        if self.listaColegios.count > 0
        {
            var colegio = listaColegios[indexPath.row]
            cell.textLabel?.text = colegio.nombreEstablecimiento
            cell.detailTextLabel?.text = "Latitud: \(colegio.latitud!), Longitud: \(colegio.longitud!)"
        }
        
        return cell
    }

    

}
