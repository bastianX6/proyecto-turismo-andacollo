//
//  ControllerSeleccionarCategoria.swift
//  Rutas Andacollo
//
//  Created by Bastián Véliz Vega on 09-04-15.
//  Copyright (c) 2015 Servicios Informaticos AionSoft Ltda. All rights reserved.
//

import UIKit

class ControllerSeleccionarCategoria: UITableViewController {

    
    var colegios : GetColegios!
    var caso : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var titulo = "Cargando datos"
        var mensaje = "Espere un momento"
        
        var alert = UIAlertController(title: titulo, message: mensaje, preferredStyle: .Alert)
        
        self.presentViewController(alert, animated: true){
            var queue = NSOperationQueue()
            
            queue.addOperationWithBlock(){
                
                self.colegios = GetColegios()
                
                NSOperationQueue.mainQueue().addOperationWithBlock(){
                    alert.dismissViewControllerAnimated(true){
                        self.tableView.reloadData()
                        self.slideMenuController()?.openLeft()
                    }
                    
                }
            }
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let colegios = self.colegios
        {
            return colegios.listaComunas.count
        }
        
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! UITableViewCell
        
        if let colegios = self.colegios
        {
            if colegios.listaComunas.count > 0
            {
                cell.textLabel?.text = colegios.listaComunas[indexPath.row]
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let colegios = self.colegios
        {
            if colegios.listaComunas.count > 0
            {
                var comuna = colegios.listaComunas[indexPath.row]
                var arrayColegios = colegios.getColegiosFromComuna(comuna)
                
                //recargar mapa o recargar tableview
                var navController = self.slideMenuController()?.mainViewController as! UINavigationController
                
                if let controller = navController.viewControllers.first as? UIViewController
                {
                    if controller is ControllerMapaCategoria
                    {
                        var controllerMapa = controller as! ControllerMapaCategoria
                        controllerMapa.listaColegios = arrayColegios
                        controllerMapa.nombreComuna = comuna
                        controllerMapa.loadAnotations()
                        self.slideMenuController()?.closeLeft()
                    }
                    else if controller is ControllerDetalleCategoria
                    {
                        var controllerDetalle = controller as! ControllerDetalleCategoria
                        controllerDetalle.listaColegios = arrayColegios
                        controllerDetalle.nombreComuna = comuna
                        controllerDetalle.tableView.reloadData()
                        
                        self.slideMenuController()?.closeLeft()
                    }
                }
            }
        }
    }
    
}
